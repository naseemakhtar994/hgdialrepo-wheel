### About HGDial ###

### Overview: ###
**This dial widget is an advanced rotation control that you can easily include into any Android project in the form of an AAR file (Androids’ equivalent of a JAR file). You can think of this Dial control as a gesture library that is just concerned with rotation; having all of the conceivable behaviour you could possibly want in a rotation control.**

**Important Developer Note about using this latest release.**

If you plan on importing this library into a project that uses an older version of this repo there have been some changes that will break your existing code. All the methods with the word 'Object' in them have been replaced with the word 'Texture'. All of the methods ending in the word 'BaseOne' have had this ending removed. The logic of the methods have not changed you only need to change the calls. Also the improved angle snapping behaviour has been migrated over from HGDialV2, angle snaps will now work intuitively when the angle is set to a number that is NOT evenly divisible by 1. This could also break existing code that uses the old angle snapping. The work around is simply to modulus by 1 any numbers you have compared against previous values returned by the angle snap methods. The developer is now satisfied with the naming conventions in this library and no more name changes are planned.

**Features of the HGDial Library:
The Features include:**

1. The ability to record the direction of rotation.
1. Allows precision rotation settings causing the dial to rotate at a different rate than the gesture (including the ability to rotate in the opposite direction of the gesture).
1. It records the number of gesture rotations.
1. It records the number of image rotations.
1. It has a cumulative dial setting. When enabled the rotation will occur relative to the touch; and disabled the rotation will start from the point where the gesture starts.
1. It has an advanced angle snap feature with an angle snap tolerance setting. The tolerance causes the dial to rotate freely until the snap tolerance is met.
1. The dial can operate in single or dual finger mode.
1. With this dial it is possible to set a minimum/maximum rotation constraint.
1. It has a variable dial behaviour causing the rotation rate to change depending on how close the gesture is the centre of the dial.
1. This library comes with a 'fling-to-spin' behaviour; having configurable fling tolerance, spin start/end speed and spin animation duration.
1. A key feature is that the dial controls are designed to interact with each other and any other widgets/layouts that implement touch listeners.
1. All of the above features play together in perfect harmony.
1. It has an array of convenience methods that greatly enhances usage scenarios.

**What could it be used for with all of these features?**

It could be used for making an analogue clock for reading and/or setting the time, It could be used as a volume control in your music or video app, it could be used as an alternative way of selecting numbers, it could be used to make a simulation of an old fashioned telephone dial, it could be used as an alternative star rating selector. Have an app that controls things remotely like a thermostat, you could use it as a custom progress dialog. The usages are only limited by your imagination.

**Notice: HGDialV2 has landed check it out: https://bitbucket.org/warwick/hg_dial_v2**

1. Enhancements include: The way one dial acts upon another (acting dials) is greatly improved, optimised and very intuitive to the developer.
1. The angle snap now functions intuitively with any angle snap angle (the angle no longer has to be evenly divisible by 1).
1. Overall major optimisations.
1. Better separation of concerns.
1. Minimised lines of code.
1. Added new usages (Can now add arrays of dial objects).
1. Added save/restore and flush state objects.
1. Can now interact with multiple dials at the same time.
1. Works fluidly with device rotation.

## See this awesome library in action! ##

**View the Youtube demonstration here: https://youtu.be/h_7VxrZ2W-g**

**Here is a list of apps that use this library:**

Demo app for this library
https://play.google.com/store/apps/details?id=com.WarwickWestonWright.HGDialDemo

Custom Date/Time Picker
https://play.google.com/store/apps/details?id=com.WarwickWestonWright.HGDateDialerDemo

Navigator control for recyclelist
https://play.google.com/store/apps/details?id=com.WarwickWestonWright.HGFastListDemo

Awesome playlist app (Demo Version)
https://play.google.com/store/apps/details?id=com.WarwickWestonWright.ABPlayListDemo

Awesome playlist app (Premium Version)
https://play.google.com/store/apps/details?id=com.WarwickWestonWright.ABPlayList

### You can make a donation the HG Widgets for Android here: ###

**https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWT2TT7X83PE8**

Should you wish to have your app added to this list or you wish to commission the services of the developer , you can email the developer at warwickwestonwright@gmail.com

## Developer Notice: ##
**Progress report for HGDialV2:**

Apart from more optimisations. The library now has a much better separation of concerns. 

1. I’ve minimised the amount of thread code for the spin-to-fling behaviour.
1. The angle snap will now behave properly when snap positions are not evenly divisible by 1. (check angle snap method has also been optimised)
1. I’ve added  a fling-to-spin behaviour that slows down on a curve. (algorithm may need a bit of tweaking)
1. I’ve added save/restore state methods.
1. But most of all the acting dials now works perfectly in a very intuitive and optimised way.
1. I’ve added two class usages. (Only one of which has been tested)
1. Made a new cog demo fragment to show off the new acting dial usage.

**ToDos:**

1. Add a behaviour that dynamically changes the duration of the spin animation based upon the speed of the spin gesture.
1. Add another 2 class usages.
1. Add a time picker to demo app. (without modifying the library, V1 Did not support this usage)
1. Add a date picker to the demo app (without modifying the library, V1 Did not support this usage)
1. Add a fast list navigator to the demo app (without modifying the library V1 DID support this usage, but I made the library anyway)

The new usages will allow for arrays of V2 objects to be dynamically added to a FrameLayout and arrays of R.drawable.ids or arrays of Bitmaps.
I won’t be finishing all of the todos before uploading V2 just the dynamic fling duration. So it’s coming very soon.
Remember the donate link. Open Source Developers need to eat and upgrade hardware too.



Key Classes in this library:
HGDialInfo: Contains the state/information returned to the developer in the callback. This status dynamically changes in response to touch events.
HGDial: The main library class.


Class Usages:
Usage 1:


```
#!java

HGDial hgDial= (HGDial) findViewById(R.id.menuIceDial);
```


XML Layout associated with Usage 1.


```
#!xml

<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
	android:id="@+id/menuIceDialContainer"
	xmlns:app="http://schemas.android.com/apk/res-auto"
	android:layout_width="wrap_content"
	android:layout_height="wrap_content">
	<com.WarwickWestonWright.HGDial.HGDial
		android:id="@+id/menuIceDial"
		android:layout_width="match_parent"
		android:layout_height="match_parent"
		app:foregroundDrawable="@drawable/menu_ice_dial"/>
</FrameLayout>
```


After constructing the necessary objects (for both usages) you need to implement the callback by calling registerCall-back on the HGDial instance as follows:

```
#!java


hgDial.registerCallback(new HGDial.IHGDial() {

	@Override
	public void onDown(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onPointerDown(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onMove(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onPointerUp(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onUp(HGDial.HGDialInfo hgDialInfo) {}

});

```

Note: As an additional classic usage you can implement HGDial.IHGDial in your class and then call hgDial.registerCallback(this);
Note: The hgDialInfo object returned by the callback contains the status of the touch event. The methods of this class that you need to be concerned with are as follows:

**LICENSE.**
This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1.     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1.     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.