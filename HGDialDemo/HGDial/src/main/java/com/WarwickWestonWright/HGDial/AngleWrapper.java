package com.WarwickWestonWright.HGDial;

import android.graphics.Point;

final class AngleWrapper {

	final Point viewCenterPoint = new Point(0, 0);
	private int gestureRotationDirection;
	private int textureRotationDirection;
	private int gestureRotationCount;
	private int textureRotationCount;
	private double gestureAngle;
	private double textureAngle;
	private double gestureTouchAngle;

	public AngleWrapper() {

		this.gestureRotationDirection = 0;
		this.textureRotationDirection = 0;
		this.gestureAngle = 0;
		this.textureAngle = 0;
		this.gestureRotationCount = 0;
		this.textureRotationCount = 0;
		this.gestureTouchAngle = 0;

	}//End public GetAngleWrapper()

	/* Accessors */
	int getGestureRotationDirection() {return this.gestureRotationDirection;}
	int getTextureRotationDirection() {return this.textureRotationDirection;}
	int getGestureRotationCount() {return this.gestureRotationCount;}
	int getTextureRotationCount() {return this.textureRotationCount;}
	double getGestureAngle() {return this.gestureAngle;}
	double getTextureAngle() {return this.textureAngle;}
	double getGestureTouchAngle() {return this.gestureTouchAngle;}

	/* Mutators */
	void setGestureRotationDirection(final int gestureRotationDirection) {this.gestureRotationDirection = gestureRotationDirection;}
	void setTextureRotationDirection(final int textureRotationDirection) {this.textureRotationDirection = textureRotationDirection;}
	void setGestureRotationCount(final int gestureRotationCount) {this.gestureRotationCount = gestureRotationCount;}
	void setTextureRotationCount(final int textureRotationCount) {this.textureRotationCount = textureRotationCount;}
	void setGestureAngle(final double gestureAngle) {this.gestureAngle = gestureAngle;}
	void setTextureAngle(final double textureAngle) {this.textureAngle = textureAngle;}
	void setGestureTouchAngle(final float gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}

}//End final class AngleWrapper