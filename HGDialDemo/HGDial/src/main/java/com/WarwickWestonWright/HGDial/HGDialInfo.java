package com.WarwickWestonWright.HGDial;

final public class HGDialInfo {

	boolean quickTap;
	int gestureRotationDirection;
	int textureRotationDirection;
	double gestureAngle;
	double textureAngle;
	float firstTouchX;
	float firstTouchY;
	float secondTouchX;
	float secondTouchY;
	int textureRotationCount;
	int gestureRotationCount;
	double gestureTouchAngle;
	boolean rotateSnapped;
	double variablePrecision;
	int minMaxRotationOutOfBounds;
	boolean spinTriggered;
	float spinCurrentSpeed;

	public HGDialInfo() {

		this.quickTap = false;
		this.gestureRotationDirection = 0;
		this.textureRotationDirection = 0;
		this.gestureAngle = 0f;
		this.textureAngle = 0f;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.textureRotationCount = 0;
		this.gestureRotationCount = 0;
		this.gestureTouchAngle = 0f;
		this.rotateSnapped = false;
		this.variablePrecision = 0f;
		this.minMaxRotationOutOfBounds = 0;
		this.spinTriggered = false;
		this.spinCurrentSpeed = 0;

	}

	/* Accessors */
	public boolean getQuickTap() {return this.quickTap;}
	public int getGestureRotationDirection() {return this.gestureRotationDirection;}
	public int getTextureRotationDirection() {return this.textureRotationDirection;}
	public double getGestureAngle() {return this.gestureAngle;}
	public double getTextureAngle() {return this.textureAngle;}
	public float getFirstTouchX() {return this.firstTouchX;}
	public float getFirstTouchY() {return this.firstTouchY;}
	public float getSecondTouchX() {return this.secondTouchX;}
	public float getSecondTouchY() {return this.secondTouchY;}
	public int getTextureRotationCount() {return this.textureRotationCount;}
	public int getGestureRotationCount() {return this.gestureRotationCount;}
	public double getGestureTouchAngle() {return this.gestureTouchAngle;}
	public boolean getRotateSnapped() {return this.rotateSnapped;}
	public double getVariablePrecision() {return this.variablePrecision;}
	public int getMinMaxRotationOutOfBounds() {return this.minMaxRotationOutOfBounds;}
	public boolean getSpinTriggered() {return this.spinTriggered;}
	public float getSpinCurrentSpeed() {return this.spinCurrentSpeed;}

	/* Mutators */
	void setQuickTap(final boolean quickTap) {this.quickTap = quickTap;}
	void setGestureRotationDirection(final int gestureRotationDirection) {this.gestureRotationDirection = gestureRotationDirection;}
	void setTextureRotationDirection(final int textureRotationDirection) {this.textureRotationDirection = textureRotationDirection;}
	void setGestureAngle(final double gestureAngle) {this.gestureAngle = gestureAngle;}
	void setTextureAngle(final double textureAngle) {this.textureAngle = textureAngle;}
	void setFirstTouchX(final float firstTouchX) {this.firstTouchX = firstTouchX;}
	void setFirstTouchY(final float firstTouchY) {this.firstTouchY = firstTouchY;}
	void setSecondTouchX(final float secondTouchX) {this.secondTouchX = secondTouchX;}
	void setSecondTouchY(final float secondTouchY) {this.secondTouchY = secondTouchY;}
	void setTextureRotationCount(final int textureRotationCount) {this.textureRotationCount = textureRotationCount;}
	void setGestureRotationCount(final int gestureRotationCount) {this.gestureRotationCount = gestureRotationCount;}
	void setGestureTouchAngle(final double gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}
	void setRotateSnapped(final boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
	void setVariablePrecision(final double variablePrecision) {this.variablePrecision = variablePrecision;}
	void setMinMaxRotationOutOfBounds(final int minMaxRotationOutOfBounds) {this.minMaxRotationOutOfBounds = minMaxRotationOutOfBounds;}
	void setSpinTriggered(final boolean spinTriggered) {this.spinTriggered = spinTriggered;}
	void setSpinCurrentSpeed(final float spinCurrentSpeed) {this.spinCurrentSpeed = spinCurrentSpeed;}

}//End final public class HGDialInfo