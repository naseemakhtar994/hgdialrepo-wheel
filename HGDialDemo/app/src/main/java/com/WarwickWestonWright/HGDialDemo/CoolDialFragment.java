/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGDialDemo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.WarwickWestonWright.HGDial.HGDial;
import com.WarwickWestonWright.HGDial.HGDialInfo;

public class CoolDialFragment extends Fragment implements HGDial.OnTouchListener {

    public interface ICoolDialFragment {void CoolDialFragmentCallback(String className);}

    public CoolDialFragment() {}

    private ICoolDialFragment iCoolDialFragment;

    private View rootView;
    private Button btnCloseCoolDialogFragment;
    private HGDial coolDialbg;
    private HGDial coolDialfg;
    private Point point;
    private FrameLayout.LayoutParams fLayoutParams;
	private RelativeLayout fgContainer;
    private Drawable foregroundDrawableBg;
    private Drawable foregroundDrawableFg;
	private double opposingAngle;
	private SharedPreferences sp;
	private HGDial.IHGDial ihgDialfg;
	private HGDial.IHGDial ihgDialbg;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.cool_dial_fragment, container, false);
        sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
		fgContainer = (RelativeLayout) rootView.findViewById(R.id.fgContainer);
        fLayoutParams = new FrameLayout.LayoutParams((point.x), (point.x), Gravity.CENTER);
        foregroundDrawableBg = ResourcesCompat.getDrawable(getResources(), R.drawable.cool_dial_bg, null);
        foregroundDrawableFg = ResourcesCompat.getDrawable(getResources(), R.drawable.cool_dial_fg, null);
        btnCloseCoolDialogFragment = (Button)rootView.findViewById(R.id.btnCloseCoolDialogFragment);
        coolDialbg = (HGDial) rootView.findViewById(R.id.coolDialBg);
        coolDialbg.setForegroundDrawable(foregroundDrawableBg);
        coolDialfg = (HGDial) rootView.findViewById(R.id.coolDialFg);
        coolDialfg.setForegroundDrawable(foregroundDrawableFg);
        coolDialbg.setLayoutParams(fLayoutParams);
        coolDialfg.setLayoutParams(fLayoutParams);

		ihgDialfg = new HGDial.IHGDial() {
			@Override
			public void onDown(HGDialInfo hgDialInfo) {}
			@Override
			public void onPointerDown(HGDialInfo hgDialInfo) {}
			@Override
			public void onMove(HGDialInfo hgDialInfo) {

				opposingAngle = -((hgDialInfo.getTextureRotationCount() + hgDialInfo.getTextureAngle()) + - 0.35f);
				coolDialbg.setRapidDial(opposingAngle);

			}

			@Override
			public void onPointerUp(HGDialInfo hgDialInfo) {}
			@Override
			public void onUp(HGDialInfo hgDialInfo) {}
		};

		ihgDialbg = new HGDial.IHGDial() {
			@Override
			public void onDown(HGDialInfo hgDialInfo) {}
			@Override
			public void onPointerDown(HGDialInfo hgDialInfo) {}
			@Override
			public void onMove(HGDialInfo hgDialInfo) {}
			@Override
			public void onPointerUp(HGDialInfo hgDialInfo) {}
			@Override
			public void onUp(HGDialInfo hgDialInfo) {}
		};

		coolDialfg.registerCallback(ihgDialfg);
		coolDialbg.registerCallback(ihgDialbg);
		fgContainer.setOnTouchListener(this);
        setupFlingValues();

        btnCloseCoolDialogFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

				getActivity().onBackPressed();

            }
        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


    private void setupFlingValues() {

        //Setup for spin-to-fling
        coolDialbg.setFlingTolerance(Integer.parseInt(sp.getString("txtFlingDistance", "200")), Long.parseLong(sp.getString("txtFlingTime", "250")));
        coolDialbg.setSpinAnimation(Float.parseFloat(sp.getString("txtStartSpeed", "10")), Float.parseFloat(sp.getString("txtEndSpeed", "0")), Long.parseLong(sp.getString("txtSpinAnimationTime", "5000")));
        coolDialfg.setFlingTolerance(Integer.parseInt(sp.getString("txtFlingDistance", "200")), Long.parseLong(sp.getString("txtFlingTime", "250")));
        coolDialfg.setSpinAnimation(Float.parseFloat(sp.getString("txtStartSpeed", "10")), Float.parseFloat(sp.getString("txtEndSpeed", "0")), Long.parseLong(sp.getString("txtSpinAnimationTime", "5000")));

    }//End private void setupFlingValues()


    @Override
    public boolean onTouch(final View v, final MotionEvent event) {

		coolDialfg.sendTouchEvent(event);

        return true;

    }//End public boolean onTouch(final View v, final MotionEvent event)


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            iCoolDialFragment = (ICoolDialFragment) activity;

        }
        catch (ClassCastException e) {

            throw new ClassCastException(activity.toString() + " must implement ICoolDialFragment");

        }

    }

    @Override
    public void onDetach() {

        super.onDetach();
        iCoolDialFragment = null;

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            coolDialbg.resetHGDial();
            coolDialfg.resetHGDial();
            setupFlingValues();

        }
        else if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            coolDialbg.resetHGDial();
            coolDialfg.resetHGDial();
            setupFlingValues();

        }//End if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)

    }//End public void onConfigurationChanged(Configuration newConfig)

}