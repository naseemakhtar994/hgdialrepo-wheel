/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGDialDemo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.WarwickWestonWright.HGDial.HGDial;
import com.WarwickWestonWright.HGDial.HGDialInfo;

public class LibraryFragment extends Fragment implements HGDial.IHGDial, DialogInterface.OnClickListener {

	public interface ILibraryFragment {void LibraryFragmentCallback();}

	public LibraryFragment() {}

	private ILibraryFragment iLibraryFragment;
	private View rootView;
    private SharedPreferences sp;
    private HGDial hgDial;
    private Drawable foregroundDrawable;
    private Point point;
    private FrameLayout.LayoutParams fLayoutParams;
    private TextView lblHGDialInfo;
    private Button btnCloseMainFragment;
    private SettingsFragment settingsFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
        point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);

    }//End public void onCreate(Bundle savedInstanceState)


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.library_activity, container, false);

		lblHGDialInfo = (TextView) rootView.findViewById(R.id.lblHGDialInfo);
		btnCloseMainFragment = (Button) rootView.findViewById(R.id.btnCloseMainFragment);
		lblHGDialInfo.setTextColor(Color.parseColor("#FF0000"));

		btnCloseMainFragment.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(iLibraryFragment != null) {iLibraryFragment.LibraryFragmentCallback();}

			}

		});

		fLayoutParams = new FrameLayout.LayoutParams((point.x), (point.x), Gravity.CENTER);
		foregroundDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.arrow, null);
		hgDial = (HGDial) rootView.findViewById(R.id.myHGDial);
		hgDial.setForegroundDrawable(foregroundDrawable);
		hgDial.setLayoutParams(fLayoutParams);
		hgDial.registerCallback(this);
		onClick(null, 0);
		//hgDial.triggerSpin(10f, 0f, 5000, 1);//Uncomment to make main demo dial spin upon opening

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	/* Top of block HGDial callbacks */
	@Override
	public void onDown(HGDialInfo hgDialInfo) {

		String dialInfo = "X: " + Float.toString(hgDialInfo.getFirstTouchX()) + ", Y: " + Float.toString(hgDialInfo.getFirstTouchY()) +
			"\nGesture Direction: " + Integer.toString(hgDialInfo.getGestureRotationDirection()) +
			"\nObject Direction: " + Integer.toString(hgDialInfo.getTextureRotationDirection()) +
			"\nGesture Rotation Count: " + Integer.toString(hgDialInfo.getGestureRotationCount()) +
			"\nGesture Angle: " + Double.toString(hgDialInfo.getGestureAngle()) +
			"\nObject Rotation Count: " + Integer.toString(hgDialInfo.getTextureRotationCount()) +
			"\nObject Angle: " + Double.toString(hgDialInfo.getTextureAngle()) +
			"\nTouch Angle: " + Double.toString(hgDialInfo.getGestureTouchAngle()) +
			"\nPrecision: " + Double.toString(hgDialInfo.getVariablePrecision());
		lblHGDialInfo.setText(dialInfo);

	}

	@Override
	public void onPointerDown(HGDialInfo hgDialInfo) {}

	@Override
	public void onMove(HGDialInfo hgDialInfo) {

		String dialInfo = "X: " + Float.toString(hgDialInfo.getFirstTouchX()) + ", Y: " + Float.toString(hgDialInfo.getFirstTouchY()) +
			"\nGesture Direction: " + Integer.toString(hgDialInfo.getGestureRotationDirection()) +
			"\nObject Direction: " + Integer.toString(hgDialInfo.getTextureRotationDirection()) +
			"\nGesture Rotation Count: " + Integer.toString(hgDialInfo.getGestureRotationCount()) +
			"\nGesture Angle: " + Double.toString(hgDialInfo.getGestureAngle()) +
			"\nObject Rotation Count: " + Integer.toString(hgDialInfo.getTextureRotationCount()) +
			"\nObject Angle: " + Double.toString(hgDialInfo.getTextureAngle()) +
			"\nTouch Angle: " + Double.toString(hgDialInfo.getGestureTouchAngle()) +
			"\nPrecision: " + Double.toString(hgDialInfo.getVariablePrecision());
		lblHGDialInfo.setText(dialInfo);

	}

	@Override
	public void onPointerUp(HGDialInfo hgDialInfo) {}

	@Override
	public void onUp(HGDialInfo hgDialInfo) {

		String dialInfo = "X: " + Float.toString(hgDialInfo.getFirstTouchX()) + ", Y: " + Float.toString(hgDialInfo.getFirstTouchY()) +
			"\nGesture Direction: " + Integer.toString(hgDialInfo.getGestureRotationDirection()) +
			"\nObject Direction: " + Integer.toString(hgDialInfo.getTextureRotationDirection()) +
			"\nGesture Rotation Count: " + Integer.toString(hgDialInfo.getGestureRotationCount()) +
			"\nGesture Angle: " + Double.toString(hgDialInfo.getGestureAngle()) +
			"\nObject Rotation Count: " + Integer.toString(hgDialInfo.getTextureRotationCount()) +
			"\nObject Angle: " + Double.toString(hgDialInfo.getTextureAngle()) +
			"\nTouch Angle: " + Double.toString(hgDialInfo.getGestureTouchAngle()) +
			"\nPrecision: " + Double.toString(hgDialInfo.getVariablePrecision());
		lblHGDialInfo.setText(dialInfo);

		if(hgDialInfo.getSpinTriggered() == true) {Toast.makeText(getActivity(), "Fling Triggered", Toast.LENGTH_LONG).show();}

		if(hgDialInfo.getQuickTap() == true) {

			//Launches Settings DialogFragment
			settingsFragment = new SettingsFragment();
			settingsFragment.setCancelable(true);
			settingsFragment.setTargetFragment(this, 0);
			settingsFragment.show(getActivity().getSupportFragmentManager(), "SettingsFragment");

		}

	}
    /* Bottom of block HGDial callbacks */


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {

			iLibraryFragment = (ILibraryFragment) activity;

		}
		catch (ClassCastException e) {

			throw new ClassCastException(activity.toString() + " must implement ILibraryFragment");

		}

	}

	@Override
	public void onDetach() {

		super.onDetach();
		iLibraryFragment = null;

	}


    @Override
    public void onClick(DialogInterface dialog, int which) {

		if(dialog != null) {dialog.dismiss();}
		setupHGView.setupView(hgDial, getActivity());

    }//End public void onClick(DialogInterface dialog, int which)

}