/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGDialDemo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.WarwickWestonWright.HGDial.HGDial;
import com.WarwickWestonWright.HGDial.HGDialInfo;

public class CogFragment extends Fragment {

    public interface ICogFragment {void cogFragmentCallback();}

    private ICogFragment iCogFragment;
	private View rootView;
	private Button btnCloseCogFragment;
    private HGDial hgDial;
    private HGDial hgDemo;
    private RelativeLayout hgOverlay;
    private RelativeLayout demoOverlay;
	private SharedPreferences sp;

    public CogFragment() {}


	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

	}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.cog_fragment, container, false);

		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
        btnCloseCogFragment = (Button) rootView.findViewById(R.id.btnCloseCogFragment);
        hgDial = (HGDial) rootView.findViewById(R.id.hgDial);
        hgDemo = (HGDial) rootView.findViewById(R.id.demoDial);
        hgOverlay = (RelativeLayout) rootView.findViewById(R.id.hgOverlay);
		demoOverlay = (RelativeLayout) rootView.findViewById(R.id.demoOverlay);
		setupFlingValues();

		hgDial.registerCallback(new HGDial.IHGDial() {
			@Override
			public void onDown(HGDialInfo hgDialInfo) {

				if(hgDemo.getSpinTriggered() == true) {hgDemo.cancelSpin();}
				hgDemo.doManualTextureDial(-(hgDialInfo.getTextureRotationCount() + hgDialInfo.getTextureAngle()));

			}

			@Override
			public void onPointerDown(HGDialInfo hgDialInfo) {}

			@Override
			public void onMove(HGDialInfo hgDialInfo) {

				hgDemo.setRapidDial(-(hgDialInfo.getTextureRotationCount() + hgDialInfo.getTextureAngle()));

			}

			@Override
			public void onPointerUp(HGDialInfo hgDialInfo) {}

			@Override
			public void onUp(HGDialInfo hgDialInfo) {

				hgDemo.doManualTextureDial(-(hgDialInfo.getTextureRotationCount() + hgDialInfo.getTextureAngle()));

			}
		});

		hgDemo.registerCallback(new HGDial.IHGDial() {
			@Override
			public void onDown(HGDialInfo hgDialInfo) {

				if(hgDial.getSpinTriggered() == true) {hgDial.cancelSpin();}
				hgDial.doManualTextureDial(-(hgDialInfo.getTextureRotationCount() + hgDialInfo.getTextureAngle()));

			}

			@Override
			public void onPointerDown(HGDialInfo hgDialInfo) {}

			@Override
			public void onMove(HGDialInfo hgDialInfo) {

				hgDial.setRapidDial(-(hgDialInfo.getTextureRotationCount() + hgDialInfo.getTextureAngle()));

			}

			@Override
			public void onPointerUp(HGDialInfo hgDialInfo) {}

			@Override
			public void onUp(HGDialInfo hgDialInfo) {

				hgDial.doManualTextureDial(-(hgDialInfo.getTextureRotationCount() + hgDialInfo.getTextureAngle()));

			}
		});

        btnCloseCogFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

				getActivity().onBackPressed();

            }

        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setupFlingValues() {

		//Setup for spin-to-fling
		hgDial.setFlingTolerance(Integer.parseInt(sp.getString("txtFlingDistance", "200")), Long.parseLong(sp.getString("txtFlingTime", "250")));
		hgDial.setSpinAnimation(Float.parseFloat(sp.getString("txtStartSpeed", "10")), Float.parseFloat(sp.getString("txtEndSpeed", "0")), Long.parseLong(sp.getString("txtSpinAnimationTime", "5000")));
		hgDemo.setFlingTolerance(Integer.parseInt(sp.getString("txtFlingDistance", "200")), Long.parseLong(sp.getString("txtFlingTime", "250")));
		hgDemo.setSpinAnimation(Float.parseFloat(sp.getString("txtStartSpeed", "10")), Float.parseFloat(sp.getString("txtEndSpeed", "0")), Long.parseLong(sp.getString("txtSpinAnimationTime", "5000")));

	}//End private void setupFlingValues()


    private Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY) {

        if(bitmap == null) {

            return null;

        }

        float xyRatio;
        int newWidth;
        int newHeight;

        if(XorY.toLowerCase().equals("x")) {

            xyRatio = (float) newDimensionXorY / bitmap.getWidth();
            newHeight = (int) (bitmap.getHeight() * xyRatio);
            bitmap = Bitmap.createScaledBitmap(bitmap, newDimensionXorY, newHeight, true);

        }
        else if(XorY.toLowerCase().equals("y")) {

            xyRatio = (float) newDimensionXorY / bitmap.getHeight();
            newWidth = (int) (bitmap.getWidth() * xyRatio);
            bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newDimensionXorY, true);

        }

        return bitmap;

    }//End private Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY)


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            iCogFragment = (ICogFragment) activity;

        }
        catch (ClassCastException e) {

            throw new ClassCastException(activity.toString() + " must implement iCogFragment");

        }

    }


    @Override
    public void onDetach() {
        super.onDetach();

        iCogFragment = null;

    }


	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

			hgDial.resetHGDial();
			hgDemo.resetHGDial();
			setupFlingValues();

		}
		else if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

			hgDial.resetHGDial();
			hgDemo.resetHGDial();
			setupFlingValues();

		}//End if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)

	}//End public void onConfigurationChanged(Configuration newConfig)

}