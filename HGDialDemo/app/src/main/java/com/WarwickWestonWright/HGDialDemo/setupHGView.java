package com.WarwickWestonWright.HGDialDemo;

import android.content.Context;
import android.content.SharedPreferences;

import com.WarwickWestonWright.HGDial.HGDial;

public class setupHGView {

	private static SharedPreferences sp;

	public static void setupView(HGDial hgDial, Context context) {

		sp = context.getSharedPreferences(context.getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);

		hgDial.setCumulativeRotate(sp.getBoolean("chkCumulativeEnable", true));
		hgDial.setIsSingleFinger(sp.getBoolean("chkSingleFingerEnable", true));

		//Setup precision rotation
		if(sp.getBoolean("chkPrecisionEnable", false) == true) {

			hgDial.setPrecisionRotation(Double.parseDouble(sp.getString("txtPrecision", "0.5")));

		}
		else if(sp.getBoolean("chkPrecisionEnable", false) == false) {

			hgDial.setPrecisionRotation(1f);

		}//End if(sp.getBoolean("chkPrecisionEnable", false) == true)

		//Setup for angle snapping
		if(sp.getBoolean("chkAngleSnapEnable", true) == true) {

			if(Double.parseDouble(sp.getString("txtAngleSnap", "0.125")) != 0) {

				if(sp.getBoolean("chkAngleSnapProximityEnable", true) == true) {

					if(Double.parseDouble(sp.getString("txtAngleSnapProximity", "0.03125")) != 0) {

						//First parameter is for angle to snap to second is for proximity
						hgDial.setAngleSnap((Double.parseDouble(sp.getString("txtAngleSnap", "0.125")) % 1), (Double.parseDouble(sp.getString("txtAngleSnapProximity", "0.03125")) % 1));

					}

				}
				else if(sp.getBoolean("chkAngleSnapProximityEnable", true) == false) {

					//When the proximity is 50%+ of the angle snap then there will be no free rotation between snaps
					hgDial.setAngleSnap((Double.parseDouble(sp.getString("txtAngleSnap", "0.125")) % 1), (Double.parseDouble(sp.getString("txtAngleSnap", "0.125")) % 1) / 2);

				}//End if(sp.getBoolean("chkAngleSnapProximityEnable", true) == true)

			}
			else if(Double.parseDouble(sp.getString("txtAngleSnap", "0.125")) == 0) {

				//For optimisation only having a proximity of zero is the same as free rotation.
				hgDial.setAngleSnap(0d, 0d);

			}//End if(Double.parseDouble(sp.getString("txtAngleSnap", "0.125")) != 0)

		}
		else if(sp.getBoolean("chkAngleSnapEnable", true) == false) {

			//First parameter is for angle to snap to second is for proximity
			hgDial.setAngleSnap(0d, 0d);

		}//End if(sp.getBoolean("chkAngleSnapEnable", true) == true)

		//Setup for min/max constraint
		if(sp.getBoolean("chkMinMaxEnable", false) == true) {

			hgDial.setMinMaxDial(Double.parseDouble(sp.getString("txtMinimumAngle", "-3.6")), Double.parseDouble(sp.getString("txtMaximumAngle", "4.4")), true);

		}
		else if(sp.getBoolean("chkMinMaxEnable", false) == false) {

			hgDial.setMinMaxDial(Double.parseDouble(sp.getString("txtMinimumAngle", "-3.6")), Double.parseDouble(sp.getString("txtMaximumAngle", "4.4")), false);

		}//End if(sp.getBoolean("chkMinMaxEnable", false) == true)

		//Setup for variable dial
		if(sp.getBoolean("chkVariableDialEnable", false) == true) {

			hgDial.setVariableDial(Double.parseDouble(sp.getString("txtVariableDialInner", "0.8")) , Double.parseDouble(sp.getString("txtVariableDialOuter", "0.8")), sp.getBoolean("chkVariableDialEnable", false));

		}
		else if(sp.getBoolean("chkVariableDialEnable", false) == false) {

			hgDial.setVariableDial(0f, 0f, sp.getBoolean("chkVariableDialEnable", false));

		}//End if(sp.getBoolean("chkVariableDialEnable", false) == true)

		if(sp.getBoolean("chkUseVariableDialCurve", false) == true) {

			hgDial.setUseVariableDialCurve(true, sp.getBoolean("chkPositiveCurve", false));

		}
		else if(sp.getBoolean("chkUseVariableDialCurve", false) == false) {

			hgDial.setUseVariableDialCurve(false, sp.getBoolean("chkPositiveCurve", false));

		}//End if(sp.getBoolean("chkUseVariableDialCurve", false) == true)

		//Setup for spin-to-fling
		if(sp.getBoolean("chkEnableFling", false) == true) {

			hgDial.setFlingTolerance(Integer.parseInt(sp.getString("txtFlingDistance", "200")), Long.parseLong(sp.getString("txtFlingTime", "250")));
			hgDial.setSpinAnimation(Float.parseFloat(sp.getString("txtStartSpeed", "10")), Float.parseFloat(sp.getString("txtEndSpeed", "0")), Long.parseLong(sp.getString("txtSpinAnimationTime", "5000")));

		}
		else if(sp.getBoolean("chkEnableFling", false) == false) {

			hgDial.setFlingTolerance(0, 0l);
			hgDial.setSpinAnimation(0f, 0f, 0l);

		}//End if(sp.getBoolean("chkEnableFling", false) == true)

	}//End public static void setupView(HGDial hgDial)

}
